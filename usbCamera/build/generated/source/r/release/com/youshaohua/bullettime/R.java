/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.youshaohua.bullettime;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int ALICEBLUE=0x7f080000;
        public static final int ANTIQUEWHITE=0x7f080001;
        public static final int AQUA=0x7f080002;
        public static final int AQUAMARINE=0x7f080003;
        public static final int AZURE=0x7f080004;
        public static final int BEIGE=0x7f080005;
        public static final int BISQUE=0x7f080006;
        public static final int BLACK=0x7f080007;
        public static final int BLANCHEDALMOND=0x7f080008;
        public static final int BLUE=0x7f080009;
        public static final int BLUEVIOLET=0x7f08000a;
        public static final int BROWN=0x7f08000b;
        public static final int BURLYWOOD=0x7f08000c;
        public static final int CADETBLUE=0x7f08000d;
        public static final int CHARTREUSE=0x7f08000e;
        public static final int CHOCOLATE=0x7f08000f;
        public static final int CLEAR=0x7f080010;
        public static final int CORAL=0x7f080011;
        public static final int CORNFLOWERBLUE=0x7f080012;
        public static final int CORNSILK=0x7f080013;
        public static final int CRIMSON=0x7f080014;
        public static final int CYAN=0x7f080015;
        public static final int DARKBLUE=0x7f080016;
        public static final int DARKCYAN=0x7f080017;
        public static final int DARKGOLDENROD=0x7f080018;
        public static final int DARKGRAY=0x7f080019;
        public static final int DARKGREEN=0x7f08001a;
        public static final int DARKKHAKI=0x7f08001b;
        public static final int DARKMAGENTA=0x7f08001c;
        public static final int DARKOLIVEGREEN=0x7f08001d;
        public static final int DARKORANGE=0x7f08001e;
        public static final int DARKORCHID=0x7f08001f;
        public static final int DARKRED=0x7f080020;
        public static final int DARKSALMON=0x7f080021;
        public static final int DARKSEAGREEN=0x7f080022;
        public static final int DARKSLATEBLUE=0x7f080023;
        public static final int DARKSLATEGRAY=0x7f080024;
        public static final int DARKTURQUOISE=0x7f080025;
        public static final int DARKVIOLET=0x7f080026;
        public static final int DEEPPINK=0x7f080027;
        public static final int DEEPSKYBLUE=0x7f080028;
        public static final int DIMGRAY=0x7f080029;
        public static final int DODGERBLUE=0x7f08002a;
        public static final int FIREBRICK=0x7f08002b;
        public static final int FLORALWHITE=0x7f08002c;
        public static final int FORESTGREEN=0x7f08002d;
        public static final int FUCHSIA=0x7f08002e;
        public static final int GAINSBORO=0x7f08002f;
        public static final int GHOSTWHITE=0x7f080030;
        public static final int GOLD=0x7f080031;
        public static final int GOLDENROD=0x7f080032;
        public static final int GRAY=0x7f080033;
        public static final int GREEN=0x7f080034;
        public static final int GREENYELLOW=0x7f080035;
        public static final int HALF_CLEAR_WHITE=0x7f080036;
        public static final int HONEYDEW=0x7f080037;
        public static final int HOTPINK=0x7f080038;
        public static final int INDIANRED=0x7f080039;
        public static final int INDIGO=0x7f08003a;
        public static final int IVORY=0x7f08003b;
        public static final int KHAKI=0x7f08003c;
        public static final int LAVENDER=0x7f08003d;
        public static final int LAVENDERBLUSH=0x7f08003e;
        public static final int LAWNGREEN=0x7f08003f;
        public static final int LEMONCHIFFON=0x7f080040;
        public static final int LIGHTBLUE=0x7f080041;
        public static final int LIGHTCORAL=0x7f080042;
        public static final int LIGHTCYAN=0x7f080043;
        public static final int LIGHTGOLDENRODYELLOW=0x7f080044;
        public static final int LIGHTGRAY=0x7f080045;
        public static final int LIGHTGREEN=0x7f080046;
        public static final int LIGHTPINK=0x7f080047;
        public static final int LIGHTSALMON=0x7f080048;
        public static final int LIGHTSEAGREEN=0x7f080049;
        public static final int LIGHTSKYBLUE=0x7f08004a;
        public static final int LIGHTSLATEGRAY=0x7f08004b;
        public static final int LIGHTSTEELBLUE=0x7f08004c;
        public static final int LIGHTYELLOW=0x7f08004d;
        public static final int LIME=0x7f08004e;
        public static final int LIMEGREEN=0x7f08004f;
        public static final int LINEN=0x7f080050;
        public static final int MAGENTA=0x7f080051;
        public static final int MAROON=0x7f080052;
        public static final int MEDIUMAQUAMARINE=0x7f080053;
        public static final int MEDIUMBLUE=0x7f080054;
        public static final int MEDIUMORCHID=0x7f080055;
        public static final int MEDIUMPURPLE=0x7f080056;
        public static final int MEDIUMSEAGREEN=0x7f080057;
        public static final int MEDIUMSLATEBLUE=0x7f080058;
        public static final int MEDIUMSPRINGGREEN=0x7f080059;
        public static final int MEDIUMTURQUOISE=0x7f08005a;
        public static final int MEDIUMVIOLETRED=0x7f08005b;
        public static final int MIDNIGHTBLUE=0x7f08005c;
        public static final int MINTCREAM=0x7f08005d;
        public static final int MISTYROSE=0x7f08005e;
        public static final int MOCCASIN=0x7f08005f;
        public static final int NAVAJOWHITE=0x7f080060;
        public static final int NAVY=0x7f080061;
        public static final int OLDLACE=0x7f080062;
        public static final int OLIVE=0x7f080063;
        public static final int OLIVEDRAB=0x7f080064;
        public static final int ORANGE=0x7f080065;
        public static final int ORANGERED=0x7f080066;
        public static final int ORCHID=0x7f080067;
        public static final int PALEGOLDENROD=0x7f080068;
        public static final int PALEGREEN=0x7f080069;
        public static final int PALETURQUOISE=0x7f08006a;
        public static final int PALEVIOLETRED=0x7f08006b;
        public static final int PAPAYAWHIP=0x7f08006c;
        public static final int PEACHPUFF=0x7f08006d;
        public static final int PERU=0x7f08006e;
        public static final int PINK=0x7f08006f;
        public static final int PLUM=0x7f080070;
        public static final int POWDERBLUE=0x7f080071;
        public static final int PURPLE=0x7f080072;
        public static final int QUATER_CLEAR_WHITE=0x7f080073;
        public static final int RED=0x7f080074;
        public static final int ROSYBROWN=0x7f080075;
        public static final int ROYALBLUE=0x7f080076;
        public static final int SADDLEBROWN=0x7f080077;
        public static final int SALMON=0x7f080078;
        public static final int SANDYBROWN=0x7f080079;
        public static final int SEAGREEN=0x7f08007a;
        public static final int SEASHELL=0x7f08007b;
        public static final int SIENNA=0x7f08007c;
        public static final int SILVER=0x7f08007d;
        public static final int SKYBLUE=0x7f08007e;
        public static final int SLATEBLUE=0x7f08007f;
        public static final int SLATEGRAY=0x7f080080;
        public static final int SNOW=0x7f080081;
        public static final int SPRINGGREEN=0x7f080082;
        public static final int STEELBLUE=0x7f080083;
        public static final int TAN=0x7f080084;
        public static final int TEAL=0x7f080085;
        public static final int THISTLE=0x7f080086;
        public static final int TOMATO=0x7f080087;
        public static final int TRANSPARENT=0x7f080088;
        public static final int TURQUOISE=0x7f080089;
        public static final int VIOLET=0x7f08008a;
        public static final int WHEAT=0x7f08008b;
        public static final int WHITE=0x7f08008c;
        public static final int WHITESMOKE=0x7f08008d;
        public static final int YELLOW=0x7f08008e;
        public static final int YELLOWGREEN=0x7f08008f;
    }
    public static final class dimen {
        public static final int activity_horizontal_margin=0x7f070000;
        public static final int activity_vertical_margin=0x7f070001;
        public static final int button_size=0x7f070002;
        public static final int horizontal_margin=0x7f070004;
        public static final int list_font_size=0x7f070003;
        public static final int list_height_min=0x7f070005;
        public static final int vertical_margin=0x7f070006;
    }
    public static final class drawable {
        public static final int border=0x7f020000;
        public static final int ic_launcher=0x7f020001;
    }
    public static final class id {
        public static final int LinearLayout1=0x7f09000e;
        public static final int RelativeLayout1=0x7f090000;
        public static final int camera_layout_BL=0x7f090007;
        public static final int camera_layout_BR=0x7f09000a;
        public static final int camera_layout_L=0x7f090001;
        public static final int camera_layout_R=0x7f090004;
        public static final int camera_view_BL=0x7f090008;
        public static final int camera_view_BR=0x7f09000b;
        public static final int camera_view_L=0x7f090002;
        public static final int camera_view_R=0x7f090005;
        public static final int disconnect=0x7f09000d;
        public static final int frame_image_BL=0x7f090009;
        public static final int frame_image_BR=0x7f09000c;
        public static final int frame_image_L=0x7f090003;
        public static final int frame_image_R=0x7f090006;
        public static final int name_text=0x7f090011;
        public static final int spinner1=0x7f090010;
        public static final int textView1=0x7f09000f;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int dialog_camera=0x7f030001;
        public static final int listitem_device=0x7f030002;
    }
    public static final class string {
        public static final int app_name=0x7f050000;
        public static final int camera=0x7f050001;
        public static final int main_activity_name=0x7f050005;
        public static final int no_device=0x7f050002;
        public static final int refresh=0x7f050003;
        public static final int select=0x7f050004;
    }
    public static final class style {
        /**  API 11 theme customizations can go here. 
 API 14 theme customizations can go here. 

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
    public static final class xml {
        public static final int device_filter=0x7f040000;
    }
}
